<?php 
/**
 * Settings
 *
 * Displays and changes website settings 
 *
 * @package GetSimple
 * @subpackage Settings
 */

# setup inclusions
$load['plugin'] = true;
include('inc/common.php');
login_cookie_check();

exec_action('load-settings');

# variable settings
$fullpath   = suggest_site_path();
$lang_array = getFiles(GSLANGPATH);

# initialize these all as null
$error = $success = $prettychck = null;
$prettyinput = '';

# if the flush cache command was invoked
if (isset($_GET['flushcache'])) {
  delete_cache();
  exec_action('flushcache'); // @hook flushcache cache was deleted
  $update = 'flushcache-success';
}

# if the undo command was invoked
if (isset($_GET['undo'])) {
  check_for_csrf("undo");
  # perform undo
  restore_datafile(GSDATAOTHERPATH . GSWEBSITEFILE);
  generate_sitemap();
  
  # redirect back to yourself to show the new restored data
  redirect('settings.php?upd=settings-restored');
}

# was the form submitted?
if(isset($_POST['submitted'])) {

  check_for_csrf("save_settings");  
    
  # website-specific fields
  if(isset($_POST['sitename'])) { 
    $SITENAME = htmlentities($_POST['sitename'], ENT_QUOTES, 'UTF-8'); 
  }
  if(isset($_POST['siteurl'])) { 
    $SITEURLNEW = tsl($_POST['siteurl']); 
  }
  if(isset($_POST['permalink'])) { 
    $PERMALINK = var_in(trim($_POST['permalink']));
  }
  if(isset($_POST['template'])) { 
    // $TEMPLATE = $_POST['template'];
  }
  if(isset($_POST['prettyurls'])) {
    $PRETTYURLS = $_POST['prettyurls'];
  } else {
    $PRETTYURLS = '';
  }
  if(isset($_POST['email'])) {
    $SITEEMAIL = var_in($_POST['email'],'email');
  }
  if(isset($_POST['timezone'])) {
    $SITETIMEZONE = var_in($_POST['timezone']);
  }
  if(isset($_POST['lang'])) {
    $SITELANG = var_in($_POST['lang']);
  }
  if(isset($_POST['about'])) {
    $SITEABOUT = var_in($_POST['about']);
  }

  // check valid lang files
  if(!in_array($SITELANG.'.php', $lang_array) and !in_array($SITELANG.'.PHP', $lang_array)) die("invalid lang"); 

  # create website xml file
  backup_datafile(GSDATAOTHERPATH . GSWEBSITEFILE);

    # udpate GSWEBSITEFILE (website.xml) file with new settings
  $xmls = getXML(GSDATAOTHERPATH.GSWEBSITEFILE,false);
  $xmls->editAddCData('SITENAME',$SITENAME);
  $xmls->editAddCData('SITEURL',$SITEURLNEW);
  $xmls->editAddCData('TEMPLATE',$TEMPLATE);
  $xmls->editAddChild('PRETTYURLS', $PRETTYURLS);
  $xmls->editAddChild('PERMALINK', var_out($PERMALINK));
  $xmls->editAddChild('EMAIL', $SITEEMAIL);
  $xmls->editAddChild('TIMEZONE', $SITETIMEZONE);
  $xmls->editAddChild('LANG', $SITELANG);
  $xmls->editAddChild('SITEUSR', $SITEUSR);
  $xmls->editAddChild('SITEABOUT', $SITEABOUT);
  
  exec_action('settings-website'); // @hook settings-website website data file before save
  
  if (! XMLsave($xmls, GSDATAOTHERPATH . GSWEBSITEFILE) ) {
    $error = i18n_r('CHMOD_ERROR');
  }

  if (!$error) {
    generate_sitemap();
    GLOBAL $SITEURLABS;
    if($SITEURLNEW !== $SITEURLABS) $SITEURLABS = $SITEURLNEW;
    // ALWAYS RELOAD ON SETTINGS SAVE, TO APPLY SITE WIDE VARAIBLE CHANGES
    redirect('settings.php?upd=settings-success');
  }
}

# are any of the control panel checkboxes checked?
$prettyinput = 'disabled';
if ($PRETTYURLS != '' ) { $prettychck = 'checked'; $prettyinput = '';}

# get all available language files
if ($SITELANG == ''){ $SITELANG = GSDEFAULTLANG; }

if (count($lang_array) != 0) {
  sort($lang_array);
  $sel = ''; $langs = '';
  foreach ($lang_array as $lfile){
    $lfile = basename($lfile,".php");
    if ($SITELANG == $lfile) { $sel="selected"; }
    $langs .= '<option '.$sel.' value="'.$lfile.'" >'.$lfile.'</option>';
    $sel = '';
  }
} else {
  $langs = '<option value="" selected="selected" >-- '.i18n_r('NONE').' --</option>';
}

$pagetitle = i18n_r('GENERAL_SETTINGS');
get_template('header');

